# HTML img.jpg
```markdown
<img src="img.jpg" alt="img">
```
<img src="img.jpg" alt="img">

# HTML ./img.jpg
```markdown
<img src="./img.jpg" alt="img">
```

<img src="./img.jpg" alt="img">

# HTML /momar/test/raw/branch/main/img.jpg
```markdown
<img src="/momar/test/raw/branch/main/img.jpg" alt="img">
```
<img src="/momar/test/raw/branch/main/img.jpg" alt="img">

# HTML /img.jpg
```markdown
<img src="/img.jpg" alt="img">
```

<img src="/img.jpg" alt="img">

# MD img.jpg
```markdown
![img](img.jpg)
```

![img](img.jpg)

# MD ./img.jpg
```markdown
![img](./img.jpg)
```

![img](./img.jpg)

# MD /momar/test/raw/branch/main/img.jpg
```markdown
![img](/momar/test/raw/branch/main/img.jpg)
```

![img](/momar/test/raw/branch/main/img.jpg)


# MD /img.jpg
```markdown
![img](/img.jpg)
```

![img](/img.jpg)

# MD with `<div align="center">` wrapper
```markdown
<div align="center">

![img](./img.jpg)

</div>
```

<div align="center">

![](./img.jpg)

</div>

